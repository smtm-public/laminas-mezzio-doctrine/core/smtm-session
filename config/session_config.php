<?php

declare(strict_types=1);

namespace Smtm\Session;

return [
    'cache_limiter' => $_ENV['SESSION_CACHE_LIMITER'] ?? '',
    'phpSaveHandler' => $_ENV['SESSION_PHP_SAVE_HANDLER'] ?? '',
    'savePath' => $_ENV['SESSION_SAVE_PATH'] ?? '',
    'serialize_handler' => $_ENV['SESSION_SERIALIZE_HANDLER'] ?? '',
    'use_trans_sid' => filter_var($_ENV['SESSION_USE_TRANS_SID'] ?? false, FILTER_VALIDATE_BOOLEAN),
    'url_rewriter_tags' => $_ENV['SESSION_URL_REWRITER_TAGS'] ?? '',
];
