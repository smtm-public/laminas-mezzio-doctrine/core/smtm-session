<?php

declare(strict_types=1);

namespace Smtm\Session;

return [
    'em' => 'default',

    'table_storage' => [
        'table_name' => 'doctrine_migrations_smtm_session',
    ],

    'migrations_paths' => [
        'Smtm\Session\Migration' => __DIR__ . '/../data/db/migrations',
    ],
];
