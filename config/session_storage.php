<?php

declare(strict_types=1);

namespace Smtm\Session;

return [
    'type' => $_ENV['SESSION_STORAGE_TYPE'] ?? '',
    'options' => [],
];
