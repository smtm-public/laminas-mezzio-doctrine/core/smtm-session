<?php

declare(strict_types=1);

namespace Smtm\Session;

use Mezzio\Session\SessionMiddleware;

return [
    [
        'priority' => 150,
        'middleware' => SessionMiddleware::class,
    ],
];
