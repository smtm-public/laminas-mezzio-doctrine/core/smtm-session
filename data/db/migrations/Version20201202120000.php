<?php

declare(strict_types=1);

namespace Smtm\Session\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120000 extends AbstractMigration
{

    use CommonMigrationTrait;

    public function up(Schema $schema): void
    {
        $this->createSessionStorageTable($schema);
    }

    public function createSessionStorageTable(Schema $schema): void
    {
        $sessionStorageTable = $schema->createTable('session_storage');
        $sessionStorageTable->addColumn('id', Types::INTEGER, ['notNull' => true])
            ->setAutoincrement(true);
        $sessionStorageTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($sessionStorageTable);
        $sessionStorageTable->addColumn('data', Types::TEXT, ['notNull' => false]);
        $this->addCreatedDatetimeImmutableColumnAndIndex($sessionStorageTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($sessionStorageTable);
        $this->addNotArchivedSmallintColumnAndIndex($sessionStorageTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($sessionStorageTable);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('session_storage');
    }
}
