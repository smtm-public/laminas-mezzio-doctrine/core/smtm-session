<?php

declare(strict_types=1);

namespace Smtm\Session;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'dependencies' => 'array',
        'middleware_pipeline' => 'array',
        'migrations' => 'array',
        'session' => 'array',
        'session_config' => 'array',
        'session_manager' => 'array',
        'session_storage' => 'array',
    ])] public function __invoke(): array
    {
        $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-session');
        $dotenv->load();

        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'middleware_pipeline' => include __DIR__ . '/../config/middlewares.php',
            'migrations' => include __DIR__ . '/../config/migrations.php',
            'session' => include __DIR__ . '/../config/session.php',
            'session_config' => include __DIR__ . '/../config/session_config.php',
            'session_manager' => include __DIR__ . '/../config/session_manager.php',
            'session_storage' => include __DIR__ . '/../config/session_storage.php',
        ];
    }
}
